provider "aws" {
  access_key = "${var.key_access}"
  secret_key = "${var.key_secret}"
  region     = "${var.az_region}"
}
module "my_vpc" {
    source      = "/root/tf-demo/modules/vpc"
    vpc_cidr    = ""
    tenancy     = "default"
#    vpc_id      = "${module.my_vpc.vpc_id}"
   #subnet_cidr = ""

}
