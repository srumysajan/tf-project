resource "aws_vpc" "demo-vpc" {
    cidr_block       = "${var.vpc_cidr}"
    instance_tenancy = "${var.tenancy}"      
    tags = {
        Name = "Demo-vpc"
    }
}

resource "aws_subnet" "demo-subnet-public-1" {
    vpc_id = "${aws_vpc.demo-vpc.id}"
    cidr_block =  "${var.demo-subnet-public-1_cidr}"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "ap-southeast-1"
    tags = {
        Name = "Demo-subnet-public-1"
    }
}

output "vpc_id" {
        value =  "${aws_vpc.demo-vpc.id}"
}




resource "aws_internet_gateway" "demo-igw" {
    vpc_id = "${aws_vpc.demo-vpc.id}"
    tags = {
        Name = "Demo-igw"
    }
}
resource "aws_route_table" "demo-public-crt" {
    vpc_id = "${aws_vpc.demo-vpc.id}"

    route {
        //associated subnet can reach everywhere
        cidr_block = "$(var.table_cidr)"
        //CRT uses this IGW to reach internet
        gateway_id = "${aws_internet_gateway.demo-igw.id}"
    }

    tags = {
        Name = "Demo-public-crt"
    }
}

resource "aws_route_table_association" "demo-crta-public-subnet-1"{
    subnet_id = "${aws_subnet.demo-subnet-public-1.id}"
    route_table_id = "${aws_route_table.demo-public-crt.id}"
}
