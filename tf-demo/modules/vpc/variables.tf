variable "vpc_cidr" {
    default= "10.0.0.0/16"
}

variable "tenancy" {
    default= "dedicated"
}


variable "demo-subnet-public-1_cidr" {
    default= "10.0.1.0/24"
}
variable "table_cidr" {
    default= "0.0.0.0/0"
}
variable "key_access" {}
variable "az_region" {}
variable "key_secret" {}


